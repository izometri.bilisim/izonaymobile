window.settings = {
    apiUrl: 'https://testapi.izonay.com/api/',
    defaultAccountCode: 'izometri',
    defaultLocale: 'tr',

    allowFilePreview: false,    //GView ile imzalanacak/onaylanacak dosyaların veya eklerin önizlemesine imkan verir. Aksi durumda dosyalar indirilir.
    

    // authentication:
    // {        
    //     adfs:
    //     {
    //         enableAzure:true
    //         enabled: true,
    //         clientId: 'e35bb17e-f412-4ab5-9d4c-10017fb3418b',
    //         authority: 'https://win-gq5hun0vh01.izometri.local/adfs',
    //         knownAuthorities: ['https://win-gq5hun0vh01.izometri.local/adfs']
    //     },
    // },  
};