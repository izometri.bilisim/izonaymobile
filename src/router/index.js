import Vue from "vue";
import VueRouter from "vue-router";
import Index from "../views/Index.vue";
import ChooseAccount from "../views/ChooseAccount.vue";
import Login from "../views/Login.vue";
import LoginWithMFA from "../views/LoginWithMFA.vue";
import Dashboard from "../views/Dashboard.vue";
import Job from "../views/Job.vue";
import JobDetail from "../views/JobDetail.vue";
import RemDetail from "../views/RemDetail.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "ChooseAccount",
    component: ChooseAccount
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/mfa",
    name: "/mfa",
    component: LoginWithMFA,
    props: true
  },
  {
    path: "/index",
    name: "Index",
    component: Index,
    children: [
      {
        path: "",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "job",
        name: "Job",
        component: Job
      },
      {
        path: "job/:id",
        name: "JobDetail",
        component: JobDetail
      },
      {
        path: "rem/:id",
        name: "RemDetail",
        component: RemDetail
      }
    ]
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
