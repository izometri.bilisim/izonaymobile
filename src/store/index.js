import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    accountCode: window.settings.accountCode,
    user: null
  },
  mutations: {
    setUser(state, user) {
      console.log("setUser: ", user);
      state.user = user;
    }
  },
  actions: {},
  modules: {}
});
