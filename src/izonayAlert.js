import Vue from "vue";

export default {
    methods: {      
        $izonayConfirm(message) {
            return Vue.swal.fire({
            title: 'İzOnay',
            text: message,
            reverseButtons: true,
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'İptal',
            customClass: {
                confirmButton: 'btn btn-lg ms-3 btn-primary',
                cancelButton: 'btn btn-lg me-3 btn-secondary'
              },
            buttonsStyling: false
        });       
        },
        $izonayAlert(message, icon = 'info') {
            return Vue.swal.fire('İzOnay',
            message,
            icon); 
        }
    }
}