import axios from "axios";

export default {
    methods:{
        async getOnlineViewPermission(){
            const {data} = await axios.get("account/GetOnlineViewPermission");
            return data;
        },
        async getRemProviders(){
            const {data} = await axios.post('/remprovider/get');
            return data;
        },
    }
}