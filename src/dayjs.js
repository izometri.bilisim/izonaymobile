import Vue from 'vue'
import dayjs from 'dayjs';
import "dayjs/locale/tr";
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);
dayjs.locale("tr");

Object.defineProperties(Vue.prototype, {
    $date: {
        get() {
            return dayjs;
        }
    }
});