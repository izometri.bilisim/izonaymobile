import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import axios from 'axios';
import msalMixin from './msalMixin'
import dataMixin from './dataMixin'
import 'sweetalert2/dist/sweetalert2.min.css';
import VueSweetalert2 from "vue-sweetalert2";
import izonayAlert from "./izonayAlert";
import './dayjs.js';

const izonay = {
  emptyObjectId: "000000000000000000000000",
  finalizerKinds :{
    Rem : { text: "KEP" }
  },
  documentTypes: {
    BankOrder: { text: "Banka Talimatı" },
    Contract: { text: "Sözleşme" },
    General: { text: "Genel" }
  },
  targetSystemKinds: {
    Ftp: { text: "FTP" },
    Sftp: { text: "sFTP" },
    Email: { text: "E-posta" },
    Rem: { text: "KEP" }
  },
  ozneKinds: {
    KurumKurulus: { text: "Kamu Kurum/Kuruluş" },
    GercekSahis: { text: "Gerçek Şahıs" },
    TuzelSahis: { text: "Tüzel Şahıs" }
  },
  ozneTypes: {
    OLUSTURAN: { text: "Oluşturan" },
    DAGITIM: { text: "Dağıtım" }
  },
  jobStatus: {
    Circulating: { text: "Dolaşımda", css: "primary" },
    Declined: { text: "Reddedildi", css: "danger" },
    Finalizing: { text: "Sonlandırılıyor", css: "info" },
    Completed: { text: "Tamamlandı", css: "success" },
    Canceled: { text: "İptal edildi", css: "warning" }
  },
  eypSecurityCodes: {
    TSD: { text: "Tasnif dışı" },
    HZO: { text: "Hizmete özel" },
    OZL: { text: "Özel" },
    GZL: { text: "Gizli" },
    CGZ: { text: "Çok gizli" },
    KSO: { text: "Kişiye Özel" }
  },
};


window.processErrorResponse = function(x, component) {
  if (!x.response) {
    component.$izonayAlert("Erişim sağlanamadı. Lütfen internet bağlantınızı kontrol edin.");
    console.log("Error Response: ", x);
    return;
  }

  var msg = "";

  switch (x.response.status) {
    case 404:
      component.$izonayAlert("Kayıt bulunamadı.");
      break;
    case 400:
      msg = window.getModelStateString(x.response.data.ModelState);

      if (x.response.data.ModelState.InvalidTokenException) {
        msg += "Lütfen yeniden oturum açın.";

        var fnRedirect = () => {
          component.$root.$data.login = null;
          window.localStorage.removeItem("user");
          axios.defaults.headers.common.token = "";

          location.href = "/";
        };

        if (component.$root.$data.login) {
          component.$izonayAlert(msg).then  (() => {
            fnRedirect();
          })
        }
        else
          fnRedirect();
        
      } else {
        component.$izonayAlert(msg);
      }
      break;
    case 401:
      msg = window.getModelStateString(x.response.data.ModelState);
      component.$izonayAlert("Yetki sorunu:\r\n" + msg);
      break;
    case 500:
      msg = window.getModelStateString(x.response.data.ModelState);
      component.$izonayAlert("Beklemediğimiz bir hata ile karşılaştık.\r\n" + msg);
      break;
    default:
      component.$izonayAlert("İşlem tamamlanamadı. Lütfen tekrar deneyin.");
      component.errorMessage = "İşlem gerçekleştirilemedi: " + x;
      break;
  }

  component.loading = false;
  return x;
};


window.getModelStateString = function(modelState) {
  var msg = "",
    i,
    k;
  for (i in modelState) {
    for (k = 0; k < modelState[i].length; k++) {
      msg += modelState[i][k] + "\r\n";
    }
  }
  return msg;
};

izonay.install = function(Vue) {
  Vue.prototype.$izonay = izonay;
};

Vue.config.productionTip = false;
Vue.use(izonay);
import CircularCountDownTimer from 'vue-circular-count-down-timer';
Vue.use(CircularCountDownTimer);
Vue.use(VueSweetalert2);
Vue.mixin(izonayAlert);

axios.defaults.baseURL = window.settings.apiUrl; // process.env.VUE_APP_API_URL;

Vue.mixin(msalMixin);
Vue.mixin(dataMixin);


Vue.mixin({
  computed: {
      isOnPrem(){
          return window.settings.isOnPrem;
      },
      getProfilePicture(){
          return this.$root?.$data?.login?.User?.Base64ProfilePicture == null ? require("./assets/no-profile.jpg") : "data:image/jpg;base64," + this.$root.$data.login.User.Base64ProfilePicture
      },
  },
  methods: {    
    hasAuth(auth){
      var list = this.$root?.$data?.login?.User?.Authorities;
      if(!list)
          return false;

      if(auth != 'Signer' && auth != 'Approved' && list.indexOf('Admin') >= 0)
          return true;

      return list.indexOf(auth) >= 0;
  },
    goBack() {
      window.history.back();
    },
    getJobTypeText(jobType){
      switch(jobType)
      {
        case "MyWaiting":
          return "İmzamı / Onayımı Bekleyen";
        case "Rem":
          return "KEP ile Gönderimimi Bekleyen";
        case "MyApproved":
          return "İmzaladığım / Onayladığım";
        case "MyDeclined":
          return "Reddetiğim";
        case "MyCanceled":
          return "İptal Ettiğim";
        default:
          return "N/A";        
      }
    },          
    getDefaultProfilePictureIfNull(profilePictureURL)
    {
        return !profilePictureURL ? require("./assets/no-profile.jpg") : profilePictureURL
    }
  }
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

// usage: {{ file.size | prettyBytes }}
Vue.filter("prettyBytes", function(num) {
  // jacked from: https://github.com/sindresorhus/pretty-bytes
  if (typeof num !== "number" || isNaN(num)) {
    return num;
  }

  var exponent;
  var unit;
  var neg = num < 0;
  var units = ["B", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb"];

  if (neg) {
    num = -num;
  }

  if (num < 1) {
    return (neg ? "-" : "") + num + " B";
  }

  exponent = Math.min(
    Math.floor(Math.log(num) / Math.log(1024)),
    units.length - 1
  );
  num = (num / Math.pow(1024, exponent)).toFixed(2) * 1;
  unit = units[exponent];

  return (neg ? "-" : "") + num + " " + unit;
});

Vue.filter("money", function(num) {
  if (isNaN(num)) return num;

  return Number(parseInt(num, 10)).toLocaleString();
});


Vue.filter('date', function(date, locale){
  if(!date)
      return null;

  if(!locale)
  {
      var login = window.localStorage.getItem('user');
      if(login)
      {
          login = JSON.parse(login);
          locale = login.Account?.Params?.DefaultDateLocale
      }
      
      if(!locale)
          locale = window.settings.defaultLocale ?? "tr";
  }

  if(typeof date == 'string' && !isNaN(date))
      date = new Date(parseInt(date, 10));

  if(!(date instanceof Date))
      date = new Date(date);

  return date.toLocaleDateString([locale]) ;
});


Vue.filter('datetime', function(date, locale){
  if(!date)
      return null;
      
  if(!locale)
  {
      var login = window.localStorage.getItem('user');
      if(login)
      {
          login = JSON.parse(login);
          locale = login.Account?.Params?.DefaultDateLocale
      }
      
      if(!locale)
          locale = window.settings.defaultLocale ?? "tr";

  }

  if(typeof date == 'string' && !isNaN(date))
      date = new Date(parseInt(date, 10));

  if(!(date instanceof Date))
      date = new Date(date);

  return date.toLocaleDateString([locale]) + ' ' + date.toLocaleTimeString([locale]);
});


Vue.filter("jobStatus", function(state) {
  if (!state) return "";
  return `<label class="badge badge-${izonay.jobStatus[state].css}">${izonay.jobStatus[state].text}</label>`;
});

Vue.filter("trackingNo", function(trackingNo) {
  var revision = parseInt(trackingNo.Revision, 10);
  if (revision && revision > 0) revision = "/" + revision;
  else revision = "";
  return `${trackingNo.Serial} - ${trackingNo.Number}${revision}`;
});

Vue.filter("checkicon", function(data) {
  if (data === true) {
    return `<svg class="bi bi-check text-success" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
            </svg><span class="d-none">Evet</span>`;
  } else if (data === false) {
    return `<svg class="bi bi-x text-danger" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"/>
        <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"/>
        </svg><span class="d-none">Hayır</span>`;
  }
});

Vue.filter("targetSystemKind", function(kind) {
  return izonay.targetSystemKinds[kind]?.text || kind;
});

Vue.filter("ozneKind", function(kind) {
  return izonay.ozneKinds[kind]?.text || kind;
});
Vue.filter("ozneTipi", function(type) {
  return izonay.ozneTypes[type]?.text || type;
});

Vue.filter("eypSecurityCode", function(code) {
  return izonay.eypSecurityCodes[code]?.text || code;
});

Vue.filter("documentType", function(docType) {
  return izonay.documentTypes[docType]?.text || docType;
});
Vue.filter("finalizerKind", function(kind) {
  return izonay.finalizerKinds[kind]?.text || kind;
});
