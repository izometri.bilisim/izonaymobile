module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        module: {
          rules: [
            {
              test: /\.js$/,
              exclude: /node_modules(?!(\/|\\)pdfjs-dist)/,
              loader: 'babel-loader',
              options: {
                  'presets': ['@babel/preset-env'],
                  'plugins': ['@babel/plugin-proposal-optional-chaining', '@babel/plugin-proposal-class-properties', '@babel/plugin-proposal-private-methods']
              }
          }
          ],
      },
      },
    pwa: {
        workboxOptions: {
            // cleanupOutdatedCaches: true,
            skipWaiting: true,
            exclude: ['Web.config'],
          }
    }
}